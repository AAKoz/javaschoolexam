package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        ArrayList<String> digits = new ArrayList<>();
        ArrayList<Character> operations = new ArrayList<>();

        if (statement == null || statement.isEmpty()) {
            return null;
        }

        for (int i = 0; i < statement.length() - 1; i++) {
            if (!Character.isDigit(statement.charAt(i)) && !Character.isDigit(statement.charAt(i + 1))) {
                if ((statement.charAt(i + 1) != '(' && statement.charAt(i) != ')' &&
                        (statement.charAt(i) != '*' || statement.charAt(i) != '/' && statement.charAt(i + 1) != '-')
                )) {
                    return null;
                }
            }
        }

        while (statement.indexOf('(') != -1) {
            if (statement.indexOf(')') == -1 || statement.indexOf(')') < statement.indexOf('(')) {
                return null;
            }
            String substatement = statement.substring(statement.indexOf('('), statement.indexOf(')') + 1);
            String substatementToCount = statement.substring(statement.indexOf('(') + 1, statement.indexOf(')'));
            String semiResult = evaluate(substatementToCount);
            if (semiResult != null) {
                statement = statement.replace(substatement, semiResult);
            } else {
                return null;
            }
        }

        String digit = "";
        int dotsCount = 0;
        for (int i = 0; i < statement.length(); i++) {
            char stringElem = statement.charAt(i);
            if (Character.isDigit(stringElem) || stringElem == '.') {
                if (stringElem == '.') {
                    dotsCount++;
                }
                if (dotsCount > 1) {
                    return null;
                }
                digit = digit + stringElem;
            } else {
                if (!digit.equals("")) {
                    digits.add(digit);
                }
                operations.add(stringElem);
                dotsCount = 0;
                digit = "";
                if ((stringElem == '*' || stringElem == '/') && statement.charAt(i + 1) == '-') {
                    digit = "-";
                    i++;
                }
            }
            if (i == statement.length() - 1) {
                digits.add(digit);
            }
        }

        String result;
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i) == '*' || operations.get(i) == '/') {
                if (operations.get(i) == '*') {
                    result = String.valueOf(
                            Double.parseDouble(digits.get(i)) * Double.parseDouble(digits.get(i + 1)));
                } else {
                    if (Double.parseDouble(digits.get(i + 1)) != 0.0) {
                        result = String.valueOf(
                                Double.parseDouble(digits.get(i)) / Double.parseDouble(digits.get(i + 1)));
                    } else {
                        return null;
                    }
                }
                digits.set(i, result);
                digits.remove(i + 1);
                operations.remove(i);
                i--;
            }
        }

        while (operations.size() != 0) {
            if (operations.get(0) == '-') {
                result = String.valueOf(
                        Double.parseDouble(digits.get(0)) - Double.parseDouble(digits.get(1)));
                digits.set(0, result);
                digits.remove(1);
                operations.remove(0);
            } else {
                if (operations.get(0) == '+') {
                    result = String.valueOf(
                            Double.parseDouble(digits.get(0)) + Double.parseDouble(digits.get(1)));
                    digits.set(0, result);
                    digits.remove(1);
                    operations.remove(0);
                } else {
                    return null;
                }
            }
        }

        result = digits.get(0);
        String lastSymbols = result.substring(result.length() - 2, result.length());
        if (lastSymbols.equals(".0")) {
            result = result.substring(0, result.length() - 2);
        }

        return result;
    }
}
