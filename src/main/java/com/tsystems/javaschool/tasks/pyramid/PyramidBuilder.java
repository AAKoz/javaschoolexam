package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int sum = 0;
        int size = 0;
        int row = 0;
        int arrayCounter = 0;

        for (int i = 0; i < 65536; i++) {
            sum += i;
            if (inputNumbers.size() == sum) {
                size = i;
                i = 65536;
            }
        }

        if (size == 0 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int[] intArray = new int[inputNumbers.size()];
        for (int i = 0; i < inputNumbers.size(); i++) {
                intArray[i] = inputNumbers.get(i);
        }

        int[][] pyramid = new int[size][size * 2 - 1];
        int leftPos = size - 1;
        int rightPos = leftPos;
        pyramid[row][leftPos] = intArray[arrayCounter];
        arrayCounter++;
        row++;
        leftPos -= 1;
        rightPos += 1;
        while (leftPos >= 0) {
            for (int i = leftPos; i <= rightPos; i = i + 2) {
                pyramid[row][i] = intArray[arrayCounter];
                arrayCounter++;
            }
            row++;
            leftPos -= 1;
            rightPos += 1;
        }

        return pyramid;
    }
}
