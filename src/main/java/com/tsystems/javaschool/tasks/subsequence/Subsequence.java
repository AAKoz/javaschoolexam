package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int counterListX = 0;

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if (x.isEmpty()) {
            return true;
        }

        for (int j = 0; j < y.size(); j++) {
            Object listElemX = x.get(counterListX);
            if (listElemX.equals(y.get(j))) {
                counterListX++;
            }
            if (counterListX == x.size()) {
                return true;
            }
        }
        return false;
    }
}
